package construcj;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;


public class FXMLProveedores {


    @FXML
    private AnchorPane panel;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtProvee;
    @FXML
    private TextField txtID;
    @FXML
    private TextField txtDireccion;
    @FXML
    private TextField txtMail;
    @FXML
    private TextField txtTelefono;
    
    @FXML
    private TableView<Proveedor> tabProveedores;
    
    
    @FXML
    void btnCargar(ActionEvent event) {
         
        String nombre = this.txtNombre.getText();
        String id = this.txtID.getText();
        String telefono = this.txtTelefono.getText();
        String direccion = this.txtDireccion.getText();
        String provee = this.txtProvee.getText();
        String mail = this.txtMail.getText();

        
        System.out.println("GUARDANDO...." );
        
        Base miBase = new Base();

        miBase.conectar();
        
        miBase.ejecutar("INSERT INTO proveedor ( nombre, id, provee, telefono, direccion,  mail) VALUES (?, ?, ? ,? ,?, ?)"
        ,nombre , id, provee, telefono, direccion, mail);

        
        txtID.setText(null);
        txtNombre.setText(null);
        txtDireccion.setText(null);
        txtTelefono.setText(null);
        txtProvee.setText(null);
        txtMail.setText(null);        
    }
    
     
    @FXML
    void onClickTabla(MouseEvent event) {
       
       
        Proveedor unProveedor = this.tabProveedores.getSelectionModel().getSelectedItem();
        //labNombre.setText(unCliente.getNombre());
    }

    @FXML
    void btnConsultar(ActionEvent event) {
       
    }
   
    public void mostrarProveedores(){
        ObservableList<Proveedor> proveedores = FXCollections.observableArrayList();
        String sql = "SELECT nombre, id, provee, telefono, direccion, mail FROM proveedor;";
        Base base = new Base();
        ArrayList<HashMap> registros = base.consultar(sql);
        
        for(HashMap reg: registros){
            proveedores.add(new Proveedor((String)reg.get("nombre"), (String)reg.get("id"), (String)reg.get("provee"),
            (String)reg.get("telefono"), (String)reg.get("direccion")
            , (String)reg.get("mail")));   
        }
       
        tabProveedores.setItems(proveedores);
    }

    public void initialize() {
        
        assert tabProveedores != null : "fx:id=\"tabProveedores\" was not injected: check your FXML file 'FXMLProveedores.fxml'.";
        //assert labNombre != null : "fx:id=\"labNombre\" was not injected: check your FXML file 'FXMLClienteNuevo.fxml'.";
        
        this.tabProveedores.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
        this.tabProveedores.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("nombre"));
        this.tabProveedores.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("provee"));
        this.tabProveedores.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("direccion"));
        this.tabProveedores.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("telefono"));
        this.tabProveedores.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("mail"));
        this.mostrarProveedores();
       
       
    }
    
    
    
  
    
}
