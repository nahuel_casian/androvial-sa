
package construcj;


public class Equipo {
    
    private String nombre_id;
    private String personal;
    private String tarea;
    private String responsable;
    
    public Equipo(String nombre_id, String personal, String tarea, String responsable){
        this.nombre_id = nombre_id;
        this.personal = personal;
        this.responsable = responsable;
        this.tarea = tarea;
    
    }
    
    public String getNombre_id(){
        return this.nombre_id;
    }
    
    public String getPersona(){
        return this.personal;
    }
    
    public String getResponsable(){
        return this.responsable;
    }
    
    public String getTarea(){
        return this.tarea;
    }
    
}
