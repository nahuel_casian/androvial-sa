package construcj;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class FXMLConstrucJInicio {
    
    @FXML
    private AnchorPane panel;

    @FXML
    void onClickbtn_Clientes(ActionEvent event) {
       this.panel.getChildren().setAll(ConstrucJ.loadFXML("FXMLCliente.fxml"));
    }
    
    @FXML
    void onClickbtn_NuevoProyecto(ActionEvent event) {
        this.panel.getChildren().setAll(ConstrucJ.loadFXML("FXMLProyectoNuevo.fxml"));
    }
    
    @FXML
    void onClickbtn_Proyectos(ActionEvent event) {
        this.panel.getChildren().setAll(ConstrucJ.loadFXML("FXMLProyectos.fxml"));
    }
    
    @FXML
    void onClickbtn_EquipoNuevo(ActionEvent event) {
        this.panel.getChildren().setAll(ConstrucJ.loadFXML("FXMLEquipoNuevo.fxml"));
    }
    
    @FXML
    void onClickbtn_Proveedores(ActionEvent event) {
        this.panel.getChildren().setAll(ConstrucJ.loadFXML("FXMLProveedores.fxml"));
    }
    
    @FXML
    void onClickbtn_Personal(ActionEvent event) {
        this.panel.getChildren().setAll(ConstrucJ.loadFXML("FXMLPersonal.fxml"));
    }

    
    @FXML
    void initialize() {
        assert panel != null : "fx:id=\"panel\" was not injected: check your FXML file 'FXMLConstrucJInicio.fxml'.";

    }    

}
                