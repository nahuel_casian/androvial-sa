
package construcj;


public class Proyecto {
    private String id;
    private String nombre;
    private String etapas;
    private String tipo_de_proyecto;
    private String direccion;
    private String presupuesto;
    private String fecha_inicio;
    private String fecha_fin;
    private String persona_a_cargo;
    private String cliente;
    private String equipo;
    
    public Proyecto(String id, String nombre, String etapas, String tipo_de_proyecto, String direccion, String presupuesto, String fecha_inicio,
        String fecha_fin, String persona_a_cargo, String cliente, String equipo){
        
        this.id = id;
        this.nombre = nombre;
        this.etapas = etapas;
        this.tipo_de_proyecto = tipo_de_proyecto;
        this.direccion = direccion;
        this.etapas = presupuesto;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.persona_a_cargo = persona_a_cargo;
        this.cliente = cliente;
        this.equipo = equipo;    
    }
    
    public String getId(){
        return this.id;
    }
    
    public String getNombre(){
        return this.nombre;
    }
    
    public String getEtapas(){
        return this.etapas;
    }
    public String getTipo_de_proyecto(){
        return this.tipo_de_proyecto;
    }
    
    public String getDireccion(){
        return this.direccion;
    }
    
    public String getPresupuesto(){
        return this.presupuesto;
    }
    
    public String getFecha_inicio(){
        return this.fecha_inicio;
    }
    
    public String getFecha_fin(){
        return this.fecha_fin;
    }
    
    public String getPrsona_a_cargo(){
        return this.persona_a_cargo;
    }
    
    public String getCliete(){
        return this.cliente;
    }
    
    public String getEquipo(){
        return this.equipo;
    }
    
    
}
