
package construcj;


public class Personal {
    
    private String legajo;
    private String nombre;
    private String dni;
    private String cargo;
    private String direccion;
    private String telefono;
    
    public Personal(String legajo, String nombre, String dni, String cargo, String direccion, String telefono){
        this.legajo = legajo;
        this.nombre = nombre;
        this.dni = dni;
        this.cargo = cargo;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public String getLegajo(){
        return this.legajo;
    }
    
    public String getNombre(){     
        return this.nombre;
    }
    
    public String getDni(){
        return this.dni;
    }
    
    public String getCargo(){     
        return this.cargo;
    }
    
    public String getDireccion(){     
        return this.direccion;
    }
    
    public String getTelefono(){     
        return this.telefono;
    }

}
    
    

