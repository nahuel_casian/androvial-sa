package construcj;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;


public class FXMLCliente {

    @FXML
    private AnchorPane panel;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtTipo;
    @FXML
    private TextField txtID;
    @FXML
    private TextField txtDireccion;
    @FXML
    private TextField txtMail;
    @FXML
    private TextField txtTelefono;
    
    @FXML
    private TableView<Cliente> tabClientes;
    
    
    @FXML
    void btnCargar(ActionEvent event) {
         
        String nombre = this.txtNombre.getText();
        String id = this.txtID.getText();
        String telefono = this.txtTelefono.getText();
        String direccion = this.txtDireccion.getText();
        String tipo = this.txtTipo.getText();
        String mail = this.txtMail.getText();

        
        System.out.println("GUARDANDO...." );
        
        Base miBase = new Base();

        miBase.conectar();
        
        miBase.ejecutar("INSERT INTO cliente ( nombre, id, direccion, telefono, tipo, mail) VALUES (?, ?, ? ,? ,?, ?)"
        ,nombre , id, direccion, telefono, tipo, mail);

        
        txtID.setText(null);
        txtNombre.setText(null);
        txtDireccion.setText(null);
        txtTelefono.setText(null);
        txtTipo.setText(null);
        txtMail.setText(null);        
    }
    
     
    @FXML
    void onClickTabla(MouseEvent event) {
       
       
        Cliente unCliente = this.tabClientes.getSelectionModel().getSelectedItem();
        //labNombre.setText(unCliente.getNombre());
    }

    @FXML
    void btnConsultar(ActionEvent event) {
       
    }
   
    public void mostrarClientes(){
        ObservableList<Cliente> clientes = FXCollections.observableArrayList();
        String sql = "SELECT nombre, id, direccion, telefono, tipo, mail FROM cliente;";
        Base base = new Base();
        ArrayList<HashMap> registros = base.consultar(sql);
        
        for(HashMap reg: registros){
            clientes.add(new Cliente((String)reg.get("nombre"), (String)reg.get("id"), (String)reg.get("direccion"),
            (String)reg.get("telefono"), (String)reg.get("tipo")
            , (String)reg.get("mail")));   
        }
       
        tabClientes.setItems(clientes);
    }

    public void initialize() {
        
        assert tabClientes != null : "fx:id=\"tabClientes\" was not injected: check your FXML file 'FXMLClienteNuevo.fxml'.";
        //assert labNombre != null : "fx:id=\"labNombre\" was not injected: check your FXML file 'FXMLClienteNuevo.fxml'.";
        
        this.tabClientes.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
        this.tabClientes.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("nombre"));
        this.tabClientes.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("tipo"));
        this.tabClientes.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("direccion"));
        this.tabClientes.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("telefono"));
        this.tabClientes.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("mail"));
        this.mostrarClientes(); 
    }

    
}
