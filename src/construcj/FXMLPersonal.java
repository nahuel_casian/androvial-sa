package construcj;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;


public class FXMLPersonal {


    @FXML
    private AnchorPane panel;
    @FXML
    private TextField txtLegajo;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtDNI;
    @FXML
    private TextField txtCargo;
    @FXML
    private TextField txtDireccion;
    @FXML
    private TextField txtTelefono;
    
    @FXML
    private TableView<Personal> tabPersonal;
    
    
    @FXML
    void btnCargar(ActionEvent event) {
         
        String legajo = this.txtLegajo.getText();
        String nombre = this.txtNombre.getText();
        String dni = this.txtDNI.getText();
        String cargo = this.txtCargo.getText();
        String direccion = this.txtDireccion.getText();
        String telefono = this.txtTelefono.getText();

        
        System.out.println("GUARDANDO...." );
        
        Base miBase = new Base();

        miBase.conectar();
        
        miBase.ejecutar("INSERT INTO personal ( legajo, nombre, dni, cargo, telefono,  direccion) VALUES (?, ?, ? ,? ,?, ?)"
        ,legajo , nombre, dni, cargo, telefono, direccion);

        txtLegajo.setText(null);
        txtNombre.setText(null);
        txtDNI.setText(null);
        txtCargo.setText(null);
        txtDireccion.setText(null);
        txtTelefono.setText(null);        
    }
    
     
    @FXML
    void onClickTabla(MouseEvent event) {
       
       
        Personal unPersonal = this.tabPersonal.getSelectionModel().getSelectedItem();
        //labNombre.setText(unCliente.getNombre());
    }

    @FXML
    void btnConsultar(ActionEvent event) {
       
    }
   
    public void mostrarPersonal(){
        ObservableList<Personal> personals = FXCollections.observableArrayList();
        String sql = "SELECT legajo, nombre, dni, cargo, telefono, direccion FROM personal;";
        Base base = new Base();
        ArrayList<HashMap> registros = base.consultar(sql);
        
        for(HashMap reg: registros){
            personals.add(new Personal((String)reg.get("legajo"), (String)reg.get("nombre")
            , (String)reg.get("dni"), (String)reg.get("cargo"), (String)reg.get("direccion")
            , (String)reg.get("telefono")));   
        }
       
        tabPersonal.setItems(personals);
    }

    public void initialize() {
        
        assert tabPersonal != null : "fx:id=\"tabPersonal\" was not injected: check your FXML file 'FXMLPersonal.fxml'.";
        //assert labNombre != null : "fx:id=\"labNombre\" was not injected: check your FXML file 'FXMLClienteNuevo.fxml'.";
        
        this.tabPersonal.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("legajo"));
        this.tabPersonal.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("nombre"));
        this.tabPersonal.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("dni"));
        this.tabPersonal.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("cargo"));
        this.tabPersonal.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("direccion"));
        this.tabPersonal.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("telefono"));
        this.mostrarPersonal();
       
       
    }
    
    
    
  
    
}
