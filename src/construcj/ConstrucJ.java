package construcj;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import java.io.IOException;


public class ConstrucJ extends Application {
    
    @Override
    public void start(Stage stage) throws Exception{
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLConstrucJInicio.fxml")));
        stage.setTitle("Carjor SRL");
        stage.setScene(scene);
        stage.show();
       
    }
   
   
    public static Parent loadFXML (String archivo) {
        try{
            return FXMLLoader.load(ConstrucJ.class.getResource(archivo));
        }
        catch(IOException e){
            System.out.println("Error no se encuentra el archivo");
            return null;
            }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       launch(args);
    }
    
}
