
package construcj;


public class Cliente {
   
    private String nombre;
    private String id;
    private String direccion;
    private String telefono;
    private String tipo;
    private String mail;
    
    public Cliente(String nombre, String id, String direccion, String telefono, String tipo, String mail){
        this.nombre = nombre;
        this.id = id;
        this.direccion = direccion;
        this.telefono = telefono;
        this.tipo = tipo;
        this.mail = mail;
    
    }
    
    public String getNombre(){
        return this.nombre;
    }
    
    public String getId(){
        return this.id;
    }
    
    public String getDireccion(){
        return this.direccion;
    }
    
    public String getTelefono(){
        return this.telefono;
    }
    
    public String getTipo(){
        return this.tipo;
    }
    
    public String getMail(){
        return this.mail;
    }
    
    
}
